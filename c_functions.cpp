// C++ functions

#include <ff++.hpp>
#include "AFunction_ext.hpp"
using namespace  Fem2D;
using namespace  std;


double putdir(KN<double> * const & Uin, KN<double> * const &Uout, KN<long> * const & indices)
{
  double n=indices->N();
  for (int ii=0;ii<n;ii++) {
    *(Uout[0]+*(indices[0]+ii))=*(Uin[0]+*(indices[0]+ii));
  }
    
  return 0;
}

class Init { public:
    Init();
};

LOADINIT(Init);  //  une variable globale qui serat construite  au chargement dynamique

Init::Init(){  // le constructeur qui ajoute la fonction "splitmesh3"  a freefem++
  Global.Add("putdir","(",new OneOperator3_<double,KN<double> *,KN<double>*,KN<long>*>(putdir));
}

