load "c_functions"
real rad=1; // radius for the circle
real timefreq = 1; // Frequency in time
real spacefreq = 1.;
real pointbywavelength = 20; // discretisation points by wavelength in time
real alpha=0.6; // Time/space discretisation parameter (<1)
real Tmax=8; // FInal time
int DIR=1; // Dirichlet = 1  ou Neumann = 0
real t0= 1.; // length of the raccord function
func g= 0*exp(-10.*sqrt((x-0.2)^2+(y-0.2)^2)); // Initial source -> 0 


// Sound speed and boundary condition functions (abstract)
func ss = 1-0.*(sqrt(x^2+y^2)<1); // Sound speed function
func dir = sin( spacefreq * atan2(y,x));


real t1=2*t0; // timing for the pulse in time (do not need it in principle)
real t2=3*t0;


// Description of the boundary
border C(t=-pi, pi) { x=rad*cos(t);  y=rad*sin(t);label=1; };

// Meshing with disc points on the boundary
real disc = 2*pi*pointbywavelength*rad*max(timefreq,spacefreq);
mesh Th = buildmesh(C(disc));

// Set the time discretisation
fespace Ph(Th,P0);
Ph h = hTriangle;
real dt=alpha*h[].min;
if (t0 < 20*h[].min) {
    cout<<"Smoothing time too short!!!"<<endl;
    exit(1);
}
real t=0;

// polynomials of degree 3 and 5 to move from 0 at time 0 to 1 at time 1
func real raccord(real t) 
{
  return(-2*t^3+3*t^2);
}
func real sp5(real t)
{
  return(6*t^5-15*t^4+10*t^3);
}


// Time dependence function
func real timedep(real t)
{
  return( raccord(t/t0) * (t<=t0) + cos(timefreq*pi*(t-t0-dt)) * (t>t0) );
}



//----------------- Finite Element Blackbox -----------------
macro grad(u) [dx(u),dy(u)] //

// Matrix for the finite elem solution
fespace Vh(Th,P1);
Vh dirvec=dir(x,y);
varf gradgrad(u,v) = int2d(Th)(grad(u)'*grad(v));
varf mass(u,v) = int2d(Th,qft=qf1pTlump)(u*v*1./ss);
varf secmb(u,v) = int1d(Th,1,qforder=2)(dir*v);
matrix R = gradgrad(Vh,Vh);
matrix M = mass(Vh,Vh);
set(M,solver=sparsesolver);

// matrix to extract the Neumann condition
varf derN(u,v) =int1d(Th,1)(grad(u)'*[N.x,N.y]*v);
real small=1.e-10;
varf massBord(u,v) =int2d(Th)(u*v*small)+int1d(Th,1)(u*v);
matrix mBord=massBord(Vh,Vh,solver=sparsesolver);
matrix RM=derN(Vh,Vh);

// Right hand sides and solutions
real[int] f(Vh.ndof),f1(Vh.ndof),f2(Vh.ndof);
f=0;f1=0;f2=0;
Vh U2=0,U1=0,U0=0,Us=0,v,dn;

func real derivNorm(real[int] sol, real[int] & out)
{
  real[int] f0(Vh.ndof);
  f0=RM*sol;
  out=mBord^-1*f0;
  return(0);
}

//-------------- Saving function --------------- 
// Write the table out in the file file assuming that out contains 
// a finite element function
func real save(real[int] out,string file)
{
 Vh out1; out1[]=out;
  {
    ofstream out(file);
    real nd=100.;
    for (int i=0;i<nd;i++) {
      real theta = 2*pi*i/nd;
      x=rad*cos(theta); y =rad*sin(theta);
      out<<theta<<" "<<out1<<endl;
    }
  }
  return 0;
}


// Hack to get the degre of freedom on the boundary
int kdfBEM=0; // nb of  DoF on border 
int[int] IdfB2Vh(0:Vh.ndof-1); // for numbering IdfB2Vh[i]==i 
{
  int lbord = 1; // label of the BEM border
  varf vbord(u1,v1) = on(1,u1=x-10);//  negative value ..
  real[int] xb=vbord(0,Vh,tgv=1);
  sort(xb,IdfB2Vh); // sort of  array  xb and IdfB2Vh
  xb = xb ? 1 : 0;//  put 1 if none zero
  kdfBEM = xb.sum +0.5; //  number of DoF on border 
  IdfB2Vh.resize(kdfBEM);  // IdfB2Vh[i] -> number DoF of border 
}

//------------- Set Initial Conditions ---------------
U0=g(x,y)*timedep(0);
if (DIR) {
  real[int] tf=dirvec[]*timedep(0);
  putdir(tf,U0[],IdfB2Vh);
  f=0;
 }
else{
  f=secmb(0,Vh); f=f*timedep(0);
 }
f2=R*U0[]; f2=f2*(-1.); f=f+f2;  f=f*(dt^2);
U1[]=M^-1*f;U1[]=U1[]+U0[];
t=dt;
if (DIR) {
  real[int] tf=dirvec[]*timedep(t);
  putdir(tf,U1[],IdfB2Vh);
}

Us = 2*U1-U0;
t=dt;
int i=0;

//--------------------- Time loop ----------------------
real[int] dnTab(Tmax/dt+1); dnTab=0; // Tab to store the newman data
int count=0;
while (t<Tmax){
  real timee;
  if (i>0)
    U0=U1; U1=U2; Us = 2*U1-U0;
  if (!DIR) {
    f=secmb(0,Vh); f=f*timedep(t);}
  else
    f=0;
  timee=clock();
  f2=R*U1[]; f2=f2*(-1.); f=f+f2;  f=f*(dt^2);
  U2[]= M^-1*f; U2[]=U2[]+Us[];
  cout<<"matrix operations:"<<clock()-timee<<endl;
  timee=clock();
  if (DIR){
    //for (int jj=0;jj<kdfBEM;jj++)
    //U2[](IdfB2Vh(jj))=dirvec[](IdfB2Vh(jj))*timedep(t);
    real[int] tf=dirvec[]*timedep(t);
    putdir(tf,U2[],IdfB2Vh);
  }
  cout<<"Dir data:"<<clock()-timee<<endl;
  i=i+1;
  t=t+dt;
  cout<<"TIME : "<<t<<endl;
   if (i%5==0){
     plot(U2,fill=1,nbiso=100,dim=2,value=1,wait=0);
   }
  if (i%1==0){
    //plot(U2,fill=1,nbiso=100,dim=3,value=1);
    derivNorm(U2[],dn[]);
    //saave(dn[],"OUT/"+i+".txt")
    x=0;y=rad;
    dnTab[count] = dn;
    count=count+1;
  }
}
